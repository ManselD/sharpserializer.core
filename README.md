# License #
This software is provided 'as-is', without any express or implied warranty. In no event will the author(s) be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:
The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
This notice may not be removed or altered from any source distribution.

Note: I didn't make this library, I just "ported" (Aka copy and paste the files into a new Net Core 2 library and change a few method calls) it.
This library was originally made by Pawel Idzikowski (http://www.sharpserializer.com)

# What does it do? #
Serializes objects into binary and vice-versa, without the use of decorators.
Great for sending data over the wire.

# Example #
		public virtual byte[] Serialise<T>(T data)
		{
			using (var ms = new MemoryStream())
			{
				var serializer = new SharpSerializer(new SharpSerializerBinarySettings()
				{
					Encoding = Encoding.Unicode,
					Mode = BinarySerializationMode.SizeOptimized,
				});
				serializer.Serialize(data, ms);
				return ms.ToArray();
			}
		}

		public virtual T Deserialise<T>(byte[] data) where T : class
		{
			using (var ms = new MemoryStream()) {
				ms.Write(data, 0, data.Length);
				ms.Position = 0;

				var serializer = new SharpSerializer(new SharpSerializerBinarySettings()
				{
					Encoding = Encoding.Unicode,
					Mode = BinarySerializationMode.SizeOptimized,
				});
				return serializer.Deserialize(ms) as T;
			}
		}